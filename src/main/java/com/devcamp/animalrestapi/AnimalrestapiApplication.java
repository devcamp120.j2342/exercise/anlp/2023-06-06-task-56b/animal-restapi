package com.devcamp.animalrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnimalrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnimalrestapiApplication.class, args);
	}

}
