package com.devcamp.animalrestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animalrestapi.models.Cat;
import com.devcamp.animalrestapi.models.Dog;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AnimalController {
    @GetMapping("/cats")
    public ArrayList<Cat> getCats() {
        ArrayList<Cat> arrListCats = new ArrayList<>();

        Cat cat1 = new Cat("Mi");
        Cat cat2 = new Cat("cho");
        Cat cat3 = new Cat("Heo");

        arrListCats.add(cat1);
        arrListCats.add(cat2);
        arrListCats.add(cat3);

        return arrListCats;
    }

    @GetMapping("/dogs")
    public ArrayList<Dog> getDogs() {

        ArrayList<Dog> arrListDogs = new ArrayList<>();

        Dog dog1 = new Dog("Ki");
        Dog dog2 = new Dog("meo");
        Dog dog3 = new Dog("heo");

        arrListDogs.add(dog1);
        arrListDogs.add(dog2);
        arrListDogs.add(dog3);

        return arrListDogs;
    }
}
